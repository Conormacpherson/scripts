#!/bin/bash
#========================================================================
#
#	FILE			:	add_file_header.sh
#
#	PROGRAMMER		:	Conor Macpherson
#
#	FIRST-VERSION	:	2018-07-07
#
#	DESCRIPTION		:	This script will add the SET file header comment
#						To the list of files provided.
#
#========================================================================


# Retrive the current system date in iso 8601 format.
DATE=`date --iso-8601`

PROJECT=$1
PROGRAMMER=$2

if [ "$1" == "--help" ]; then

	echo -e "Script Usage:\n<Project Name> <Programmer Name> <Files(...)>"
	echo -e "... is a list of files."
	echo -e "No files will be created. Only files which aready exist will be edited."

fi


while [ ! "$3" == "" ]; do

	if [ -e $3 ]; then

		echo -e "/*!\n *\n *\t@file\n *\tFILE\t\t\t:\t$3\n *\n *\tPROJECT\t\t\t:\t${PROJECT}
 *\n *\tPROGRAMMER\t\t:\t${PROGRAMMER}\n *\n *\tFIRST-VERSION\t:\t${DATE}\n *\n *\tDESCRIPTION\t\t:\t
 *\n */" | cat - $3 > temp && mv temp $3

	else

		echo "Invalid input. File: $3 does not exits."
	fi

	shift

done
