#! /bin/bash
#=============================================================================================
#
#	FILE		:	SET_MES_SETUP.sh
#
#	PROGRAMMER	:	Conor Macpherson
#
#	DESCRIPTION	:	Sets up the linux denvironment for development on the STM32f3
#				Microcontroller used in Conestoga's SET - MES class.
#
#	FIRST-VERSION	:	2018-06-20
#
#==============================================================================================

unlock=$1

if [ "$1" == "-u" ];
then

	echo "ps -ef | grep apt"

	# Find all apt processes running causing an apt-lock.
	grep_content=$(ps -ef | grep apt)

	echo ${grep_content}

	# kill each apt process
	for find in `echo ${grep_content} | grep -o -P "\d{5}"`
	do
		echo sudo kill ${find}
		sudo kill ${find}
	done
fi

echo "sudo apt-get update"
sudo apt-get update

echo "sudo apt-get installo libftdi-dev libusb-1.0-0-dev openocd minicom gcc-arm-none-eabi gdb-arm-none-eabi vim"
sudo apt-get install libftdi-dev libusb-1.0-0-dev openocd minicom gcc-arm-none-eabi gdb-arm-none-eabi vim

echo "which openocd"
which openocd

echo "openocd --version"
openocd --version

echo "sudo apt-get remove modemmanager"
sudo apt-get remove modemmanager

echo "wget http://c758482.r82.cf2.rackcdn.com/sublime-text_build-3083_amd64.deb"
wget http://c758482.r82.cf2.rackcdn.com/sublime-text_build-3083_amd64.deb

echo "sudo dpkg -i sublime-text_build-3083_amd64.deb"
sudo dpkg -i sublime-text_build-3083_amd64.deb

echo "sudo apt install git"
sudo apt install git

echo "git config --add user.email Cmacpherson1766@conestogac.on.ca"
git config --add user.email Cmacpherson1766@conestogac.on.ca

echo "git config --add user.name Conormacpherson"
git config --add user.name Conormacpherson
